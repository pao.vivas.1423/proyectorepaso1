import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  showAllType: boolean;
  showType: string;
  buildings = [
    { id: 1,
      name: 'Floride Chemicals Factory',
      img: 'https://colorlib.com/preview/theme/constructioncompany/assets/img/gallery/project1.png', 
      type: 'intorior'
    },
    { id: 2,
      name: 'Floride Chemicals Factory',
      img: 'https://colorlib.com/preview/theme/constructioncompany/assets/img/gallery/project2.png',
      type: 'recent'
    },
    { id: 3,
      name:'Floride Chemicals Factory',
      img: 'https://colorlib.com/preview/theme/constructioncompany/assets/img/gallery/project3.png',
      type: 'bigBuilding'
    },
    { id: 4,
      name: 'Floride Chemicals Factory',
      img: 'https://colorlib.com/preview/theme/constructioncompany/assets/img/gallery/project4.png',
      type: 'park'
    },
    { id: 5,
      name: 'Floride Chemicals Factory',
      img: 'https://colorlib.com/preview/theme/constructioncompany/assets/img/gallery/project5.png',
      type: 'intorior'
    },
    { id: 6,
      name: 'Floride Chemicals Factory',
      img: 'https://colorlib.com/preview/theme/constructioncompany/assets/img/gallery/project6.png',
      type: 'recent'
    },
  ];

  constructor() { }

  ngOnInit(): void {
    this.showAll();
  }

  showAll(){
    this.showAllType = true;
    this.showType = 'all';
  }

  showIntorior(){
    this.showType = 'intorior';
    this.showAllType = false;
  }

  showRecent(){
    this.showType = 'recent';
    this.showAllType = false;
  }
  
  showBigBuilding(){
    this.showType = 'bigBuilding';
    this.showAllType = false;
  }
  
  showPark(){
    this.showType = 'park';
    this.showAllType = false;
  }

}
