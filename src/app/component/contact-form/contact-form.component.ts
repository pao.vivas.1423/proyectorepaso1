import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  name: string;
  subject: string;
  email: string;
  message: string;

  constructor(

  ) {}

  ngOnInit(): void {
  }

  processForm() {
    const allInfo = `My name is ${this.name}. My email is ${this.email}. My subject is ${this.subject}.  My message is ${this.message}`;
    alert(allInfo);
  }
}
