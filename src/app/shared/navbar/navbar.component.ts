import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const form = new FormGroup({ 
      first: new FormControl('Nancy', Validators.minLength(2)),
      last: new FormControl('Drew'),
    });
  }

  goToContactForm() {
    
  }

}
