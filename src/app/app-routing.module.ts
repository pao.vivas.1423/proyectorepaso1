import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './component/principal/principal.component';
import { Page404Component } from './component/page404/page404.component';
import { AboutComponent } from './component/about/about.component';
import { ContactFormComponent } from './component/contact-form/contact-form.component';


const routes: Routes = [
  {path: '', component: PrincipalComponent},
  {path: 'home', component: PrincipalComponent},
  {path: 'contact', component: ContactFormComponent},
  {path: 'about', component: AboutComponent},
  {path: '**', component: Page404Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
